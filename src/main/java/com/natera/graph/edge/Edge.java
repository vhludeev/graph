package com.natera.graph.edge;

/**
 * Edge between two vertices
 *
 * @author Khludeev Vitalii
 * @since 27.06.2019
 */
public interface Edge<V>
{
    /**
     * get first vertex
     *
     * @return first vertex
     */
    V getVertex1();

    /**
     * get second vertex
     *
     * @return second vertex
     */
    V getVertex2();
}
