package com.natera.graph.edge;

/**
 * @author Khludeev Vitalii
 * @since 22.07.2019
 */
public class DefaultWeightedEdge<V> extends DefaultEdge<V> implements WeightedEdge<V>
{
    private final int weight;

    public DefaultWeightedEdge(V vertex1, V vertex2, int weight)
    {
        super(vertex1, vertex2);
        this.weight = weight;
    }

    @Override
    public int getWeight()
    {
        return weight;
    }

    @Override
    public String toString()
    {
        return "DefaultWeightedEdge{" +
                "weight=" + weight +
                "} " + super.toString();
    }
}
