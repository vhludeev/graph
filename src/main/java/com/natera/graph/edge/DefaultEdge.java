package com.natera.graph.edge;

/**
 * Default unweighted edge
 *
 * @author Khludeev Vitalii
 * @since 30.06.2019
 */
public class DefaultEdge<V> implements Edge<V>
{
    private final V vertex1;

    private final V vertex2;

    public DefaultEdge(V vertex1, V vertex2)
    {
        this.vertex1 = vertex1;
        this.vertex2 = vertex2;
    }

    @Override
    public V getVertex1()
    {
        return vertex1;
    }

    @Override
    public V getVertex2()
    {
        return vertex2;
    }

    @Override
    public String toString()
    {
        return "DefaultEdge{" +
                "vertex1=" + vertex1 +
                ", vertex2=" + vertex2 +
                '}';
    }
}
