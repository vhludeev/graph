package com.natera.graph.edge;

/**
 * @author Khludeev Vitalii
 * @since 22.07.2019
 */
public interface WeightedEdge<V> extends Edge<V>
{
    int getWeight();
}
