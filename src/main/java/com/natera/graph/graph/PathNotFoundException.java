package com.natera.graph.graph;

/**
 * Exception which occurs when path between two vertices not found
 *
 * @author Khludeev Vitalii
 * @since 01.07.2019
 */
public class PathNotFoundException extends Exception
{
    public PathNotFoundException(Object src, Object dest)
    {
        super(String.format("Path between %s and %s not found", src, dest));
    }
}
