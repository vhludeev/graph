package com.natera.graph.graph;

import com.natera.graph.edge.Edge;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author Khludeev Vitalii
 * @since 22.07.2019
 */
public abstract class AbstractGraph <V, E extends Edge<V>>implements Graph<V, E>
{
    protected final Map<V, List<E>> matrix = new HashMap<>();

    protected final boolean isDirected;

    public AbstractGraph()
    {
        this.isDirected = false;
    }

    public AbstractGraph(boolean isDirected)
    {
        this.isDirected = isDirected;
    }

    @Override
    public void addVertex(V vertex)
    {
        if (matrix.containsKey(vertex))
        {
            throw new IllegalArgumentException(String.format("Vertex %s already exists in graph", vertex));
        }
        matrix.put(vertex, new ArrayList<>());
    }

    @Override
    public void addEdge(E edge)
    {
        String exMessageFormat = "Vertex %s does not exist in graph";
        if (!matrix.containsKey(edge.getVertex1()))
        {
            throw new IllegalArgumentException(String.format(exMessageFormat, edge.getVertex1()));
        }
        else if (!matrix.containsKey(edge.getVertex2()))
        {
            throw new IllegalArgumentException(String.format(exMessageFormat, edge.getVertex2()));
        }

        List<E> destinations = matrix.get(edge.getVertex1());
        if (destinations.stream().anyMatch(e -> Objects.equals(edge.getVertex2(), getDestination(edge.getVertex1(), e))))
        {
            throw new IllegalArgumentException(
                    String.format(
                            "Edge between %s and %s already exists in graph. Is graph directed: %s",
                            edge.getVertex1(), edge.getVertex2(), isDirected));
        }
        destinations.add(edge);
        if (!isDirected)
        {
            matrix.get(edge.getVertex2()).add(edge);
        }
    }

    /**
     * Due to the fact that in undirected mode the same edge associates with both
     * vertices, we need define which vertex in edge is the destination
     *
     * @param source source vertex
     * @param edge   edge
     * @return vertex which is the destination
     */
    protected V getDestination(V source, E edge)
    {
        if (isDirected || Objects.equals(edge.getVertex1(), source))
        {
            return edge.getVertex2();
        }
        return edge.getVertex1();
    }
}
