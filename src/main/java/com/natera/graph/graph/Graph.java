package com.natera.graph.graph;

import com.natera.graph.edge.Edge;

import java.util.List;

/**
 * Graph which supports operations with vertices and edges between them
 *
 * @author Khludeev Vitalii
 * @since 27.06.2019
 */
public interface Graph<V, E extends Edge<V>>
{
    /**
     * Add vertex to graph
     *
     * @param vertex vertex
     * @throws IllegalArgumentException if implementation prevents adding existed vertex
     */
    void addVertex(V vertex);

    /**
     * Add edge between two vertexes
     *
     * @param edge edge between two vertices
     * @throws IllegalArgumentException occurs in the following cases:
     *                                  1. At least one vertex in edge does not exist in graph
     *                                  2. If implementation prevents adding existed edge between two vertexes
     */
    void addEdge(E edge);

    /**
     * Find path from source to destination
     *
     * @param source      source
     * @param destination destination
     * @return list which contains all vertices met in path between source and destination.
     * The list contains the destination at the end but does not contain the
     * source at the beginning
     * @throws PathNotFoundException    if path from source to destination not found
     * @throws IllegalArgumentException if source or destination does not exist in graph
     */
    List<V> getPath(V source, V destination) throws PathNotFoundException;
}
