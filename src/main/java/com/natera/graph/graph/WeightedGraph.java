package com.natera.graph.graph;

import com.natera.graph.edge.WeightedEdge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;

/**
 * @author Khludeev Vitalii
 * @since 22.07.2019
 */
public class WeightedGraph<V, E extends WeightedEdge<V>> extends AbstractGraph<V, E>
{
    public WeightedGraph()
    {
        super(false);
    }

    public WeightedGraph(boolean isDirected)
    {
        super(isDirected);
    }

    @Override
    public List<V> getPath(V source, V destination) throws PathNotFoundException
    {
        if (!matrix.containsKey(source))
        {
            throw new IllegalArgumentException(String.format("Source %s does not exist in graph", source));
        }
        else if (!matrix.containsKey(destination))
        {
            throw new IllegalArgumentException(String.format("Destination %s does not exist in graph", destination));
        }
        Map<V, List<E>> paths = new HashMap<>();
        Queue<V> queue = new LinkedList<>();
        queue.add(source);
        if (!source.equals(destination))
        {
            paths.put(source, Collections.emptyList());
        }
        while (!queue.isEmpty())
        {
            V vertex = queue.poll();
            for (E edge : matrix.get(vertex))
            {
                List<E> parentPath = paths.get(vertex);
                List<E> path = new ArrayList<>((parentPath != null ? parentPath.size() : 0) + 1);
                if (parentPath != null)
                {
                    path.addAll(parentPath);
                }
                path.add(edge);
                if (Objects.equals(edge, source))
                {
                    continue;
                }
                V nextDestination = getDestination(vertex, edge);
                if (!paths.containsKey(nextDestination))
                {
                    queue.add(nextDestination);
                    paths.put(nextDestination, path);
                    continue;
                }
                int oldWeight = paths.get(nextDestination).stream().map(WeightedEdge::getWeight).reduce(Integer::sum).orElse(0);
                int newWeight = path.stream().map(WeightedEdge::getWeight).reduce(Integer::sum).orElse(0);
                if (newWeight < oldWeight)
                {
                    paths.put(nextDestination, path);
                    queue.add(nextDestination);
                }
            }
        }
        if (paths.containsKey(destination))
        {
            List<V> collect = new ArrayList<>(paths.get(destination).size());
            V v = source;
            for (E e : paths.get(destination))
            {
                V dest = getDestination(v, e);
                collect.add(dest);
                v = dest;
            }

            return collect;
        }
        throw new PathNotFoundException(source, destination);
    }
}
