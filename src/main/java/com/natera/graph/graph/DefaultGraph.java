package com.natera.graph.graph;

import com.natera.graph.edge.Edge;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.stream.Collectors;

/**
 * Default unweighted graph implementation. This graph can work in two modes.
 * This graph can be directed or undirected. All information about all
 * vertices and edges between them keeps in {@link Map} matrix. The keys in matrix
 * are the vertices. Values ​​are the edges associated with the corresponding vertex.
 * In undirected mode the same edge associates with both vertices in graph.
 * In directed mode the edge associates with only source vertex.
 *
 * @author Khludeev Vitalii
 * @since 26.06.2019
 */
public class DefaultGraph<V, E extends Edge<V>> extends AbstractGraph<V, E>
{
    public DefaultGraph()
    {
        super(false);
    }

    public DefaultGraph(boolean isDirected)
    {
        super(isDirected);
    }

    /**
     * Traverses the graph from source until path will not be found or all vertices
     * which inextricably linked with the source will not be visited. The method
     * prevents loops occurring in the found path.
     *
     * @param source      source
     * @param destination destination
     * @return first found (maybe not optimal) path
     * @throws PathNotFoundException if path not found
     */
    @Override
    public List<V> getPath(V source, V destination) throws PathNotFoundException
    {
        if (!matrix.containsKey(source))
        {
            throw new IllegalArgumentException(String.format("Source %s does not exist in graph", source));
        }
        else if (!matrix.containsKey(destination))
        {
            throw new IllegalArgumentException(String.format("Destination %s does not exist in graph", destination));
        }
        Map<V, List<V>> paths = new HashMap<>();
        Queue<V> queue = new LinkedList<>();
        queue.add(source);
        while (!queue.isEmpty())
        {
            V vertex = queue.poll();
            List<V> destinations = matrix.get(vertex).stream()
                    .map(e -> getDestination(vertex, e))
                    .filter(v -> !paths.containsKey(v))
                    .collect(Collectors.toList());
            for (V v : destinations)
            {
                List<V> parentPath = paths.get(vertex);
                List<V> path = new ArrayList<>((parentPath != null ? parentPath.size() : 0) + 1);
                if (parentPath != null)
                {
                    path.addAll(parentPath);
                }
                path.add(v);
                if (Objects.equals(v, destination))
                {
                    return path;
                }
                else if (Objects.equals(v, source))
                {
                    continue;
                }
                paths.put(v, path);
                queue.add(v);
            }
        }
        throw new PathNotFoundException(source, destination);
    }
}
