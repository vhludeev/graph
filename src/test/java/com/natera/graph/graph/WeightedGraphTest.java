package com.natera.graph.graph;

import com.natera.graph.edge.DefaultWeightedEdge;
import com.natera.graph.edge.WeightedEdge;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.testng.Assert.assertEquals;

/**
 * @author Khludeev Vitalii
 * @since 22.07.2019
 */
public class WeightedGraphTest
{
    @Test
    public void testGetPath() throws PathNotFoundException
    {
        Graph<Integer, WeightedEdge<Integer>> graph = new WeightedGraph<>(true);
        graph.addVertex(5);
        graph.addVertex(7);
        graph.addVertex(1);
        graph.addVertex(4);
        graph.addVertex(17);
        graph.addVertex(2);
        graph.addVertex(6);
        graph.addVertex(14);
        graph.addVertex(12);
        graph.addVertex(11);
        graph.addVertex(10);
        graph.addVertex(8);
        graph.addVertex(3);
        graph.addVertex(16);
        graph.addEdge(new DefaultWeightedEdge<>(5, 5, 3));
        graph.addEdge(new DefaultWeightedEdge<>(5, 7, 3));
        graph.addEdge(new DefaultWeightedEdge<>(7, 1, 3));
        graph.addEdge(new DefaultWeightedEdge<>(1, 4, 3));
        graph.addEdge(new DefaultWeightedEdge<>(4, 17, 3));
        graph.addEdge(new DefaultWeightedEdge<>(5, 2, 1));
        graph.addEdge(new DefaultWeightedEdge<>(2, 6, 6));
        graph.addEdge(new DefaultWeightedEdge<>(6, 14, 3));
        graph.addEdge(new DefaultWeightedEdge<>(14, 12, 1));
        graph.addEdge(new DefaultWeightedEdge<>(12, 17, 1));
        graph.addEdge(new DefaultWeightedEdge<>(6, 11, 2));
        graph.addEdge(new DefaultWeightedEdge<>(11, 10, 2));
        graph.addEdge(new DefaultWeightedEdge<>(5, 10, 1));
        graph.addEdge(new DefaultWeightedEdge<>(10, 8, 1));
        graph.addEdge(new DefaultWeightedEdge<>(8, 3, 1));
        graph.addEdge(new DefaultWeightedEdge<>(3, 16, 1));
        graph.addEdge(new DefaultWeightedEdge<>(16, 17, 8));
        graph.addEdge(new DefaultWeightedEdge<>(16, 6, 2));
        assertEquals(graph.getPath(5, 17), Arrays.asList(10, 8, 3, 16, 6, 14, 12, 17));
    }

    @Test
    public void testGetPathLoopToSelf() throws PathNotFoundException
    {
        Graph<Integer, WeightedEdge<Integer>> graph = new WeightedGraph<>(true);
        graph.addVertex(5);
        graph.addVertex(7);
        graph.addVertex(1);
        graph.addVertex(4);
        graph.addVertex(17);
        graph.addVertex(2);
        graph.addVertex(6);
        graph.addVertex(14);
        graph.addVertex(12);
        graph.addVertex(11);
        graph.addVertex(10);
        graph.addVertex(8);
        graph.addVertex(3);
        graph.addVertex(16);
        graph.addEdge(new DefaultWeightedEdge<>(5, 5, 3));
        graph.addEdge(new DefaultWeightedEdge<>(5, 7, 3));
        graph.addEdge(new DefaultWeightedEdge<>(7, 1, 3));
        graph.addEdge(new DefaultWeightedEdge<>(1, 4, 3));
        graph.addEdge(new DefaultWeightedEdge<>(4, 17, 3));
        graph.addEdge(new DefaultWeightedEdge<>(5, 2, 1));
        graph.addEdge(new DefaultWeightedEdge<>(2, 6, 6));
        graph.addEdge(new DefaultWeightedEdge<>(6, 14, 3));
        graph.addEdge(new DefaultWeightedEdge<>(14, 12, 1));
        graph.addEdge(new DefaultWeightedEdge<>(12, 17, 1));
        graph.addEdge(new DefaultWeightedEdge<>(6, 11, 2));
        graph.addEdge(new DefaultWeightedEdge<>(11, 10, 2));
        graph.addEdge(new DefaultWeightedEdge<>(5, 10, 1));
        graph.addEdge(new DefaultWeightedEdge<>(10, 8, 1));
        graph.addEdge(new DefaultWeightedEdge<>(8, 3, 1));
        graph.addEdge(new DefaultWeightedEdge<>(3, 16, 1));
        graph.addEdge(new DefaultWeightedEdge<>(16, 17, 8));
        graph.addEdge(new DefaultWeightedEdge<>(16, 6, 2));
        assertEquals(graph.getPath(5, 5), Collections.singletonList(5));
    }
}