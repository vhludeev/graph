package com.natera.graph.graph;

import com.natera.graph.edge.DefaultEdge;
import com.natera.graph.edge.Edge;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Khludeev Vitalii
 * @since 01.07.2019
 */
public class DefaultGraphTest
{
    @Test
    public void getPathDirectedGraphSuccess() throws PathNotFoundException
    {
        Graph<Integer, Edge<Integer>> graph = new DefaultGraph<>(true);
        graph.addVertex(4);
        graph.addVertex(8);
        graph.addVertex(6);
        graph.addVertex(2);
        graph.addVertex(1);
        graph.addVertex(5);
        graph.addEdge(new DefaultEdge<>(4, 6));
        graph.addEdge(new DefaultEdge<>(8, 4));
        graph.addEdge(new DefaultEdge<>(6, 1));
        graph.addEdge(new DefaultEdge<>(8, 2));
        graph.addEdge(new DefaultEdge<>(1, 2));
        graph.addEdge(new DefaultEdge<>(5, 1));
        graph.addEdge(new DefaultEdge<>(2, 5));
        Assert.assertEquals(Arrays.asList(6, 1, 2, 5), graph.getPath(4, 5));
    }

    @Test
    public void getPathUndirectedGraphSuccess() throws PathNotFoundException
    {
        Graph<Integer, Edge<Integer>> graph = new DefaultGraph<>();
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addVertex(3);
        graph.addVertex(4);
        graph.addVertex(5);
        graph.addVertex(6);
        graph.addVertex(7);
        graph.addEdge(new DefaultEdge<>(1, 2));
        graph.addEdge(new DefaultEdge<>(1, 3));
        graph.addEdge(new DefaultEdge<>(3, 4));
        graph.addEdge(new DefaultEdge<>(4, 1));
        graph.addEdge(new DefaultEdge<>(4, 5));
        graph.addEdge(new DefaultEdge<>(2, 6));
        graph.addEdge(new DefaultEdge<>(6, 5));
        graph.addEdge(new DefaultEdge<>(6, 7));

        List<List<Integer>> expectedPaths = new ArrayList<>();
        expectedPaths.add(Arrays.asList(2, 6, 5));
        expectedPaths.add(Arrays.asList(4, 5));
        expectedPaths.add(Arrays.asList(3, 4, 5));

        Assert.assertTrue(expectedPaths.contains(graph.getPath(1, 5)));
    }

    @Test
    public void nullableSupportInUndirectedGraph() throws PathNotFoundException
    {
        Graph<Integer, Edge<Integer>> graph = new DefaultGraph<>(true);
        graph.addVertex(null);
        graph.addVertex(4);
        graph.addVertex(8);
        graph.addVertex(6);
        graph.addVertex(2);
        graph.addVertex(1);
        graph.addVertex(5);
        graph.addEdge(new DefaultEdge<>(null, 4));
        graph.addEdge(new DefaultEdge<>(4, 6));
        graph.addEdge(new DefaultEdge<>(8, 4));
        graph.addEdge(new DefaultEdge<>(6, 1));
        graph.addEdge(new DefaultEdge<>(8, 2));
        graph.addEdge(new DefaultEdge<>(1, 2));
        graph.addEdge(new DefaultEdge<>(5, 1));
        graph.addEdge(new DefaultEdge<>(2, 5));
        Assert.assertEquals(Arrays.asList(4, 6, 1, 2, 5), graph.getPath(null, 5));
    }

    @Test
    public void nullableSupportInDirectedGraphInSource() throws PathNotFoundException
    {
        Graph<Integer, Edge<Integer>> graph = new DefaultGraph<>(true);
        graph.addVertex(null);
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addVertex(3);
        graph.addVertex(4);
        graph.addVertex(5);
        graph.addVertex(6);
        graph.addVertex(7);
        graph.addEdge(new DefaultEdge<>(null, 1));
        graph.addEdge(new DefaultEdge<>(1, 2));
        graph.addEdge(new DefaultEdge<>(1, 3));
        graph.addEdge(new DefaultEdge<>(3, 4));
        graph.addEdge(new DefaultEdge<>(4, 1));
        graph.addEdge(new DefaultEdge<>(4, 5));
        graph.addEdge(new DefaultEdge<>(2, 6));
        graph.addEdge(new DefaultEdge<>(6, 5));
        graph.addEdge(new DefaultEdge<>(6, 7));

        List<List<Integer>> expectedPaths = new ArrayList<>();
        expectedPaths.add(Arrays.asList(1, 2, 6, 5));
        expectedPaths.add(Arrays.asList(1, 4, 5));
        expectedPaths.add(Arrays.asList(1, 3, 4, 5));

        Assert.assertTrue(expectedPaths.contains(graph.getPath(null, 5)));
    }

    @Test
    public void nullableSupportInDirectedGraphInDestination() throws PathNotFoundException
    {
        Graph<Integer, Edge<Integer>> graph = new DefaultGraph<>(true);
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addVertex(3);
        graph.addVertex(4);
        graph.addVertex(5);
        graph.addVertex(6);
        graph.addVertex(7);
        graph.addVertex(null);
        graph.addEdge(new DefaultEdge<>(1, 2));
        graph.addEdge(new DefaultEdge<>(1, 3));
        graph.addEdge(new DefaultEdge<>(3, 4));
        graph.addEdge(new DefaultEdge<>(4, 1));
        graph.addEdge(new DefaultEdge<>(4, 5));
        graph.addEdge(new DefaultEdge<>(2, 6));
        graph.addEdge(new DefaultEdge<>(6, 5));
        graph.addEdge(new DefaultEdge<>(6, 7));
        graph.addEdge(new DefaultEdge<>(5, null));

        List<List<Integer>> expectedPaths = new ArrayList<>();
        expectedPaths.add(Arrays.asList(2, 6, 5, null));
        expectedPaths.add(Arrays.asList(4, 5, null));
        expectedPaths.add(Arrays.asList(3, 4, 5, null));

        Assert.assertTrue(expectedPaths.contains(graph.getPath(1, null)));
    }

    @Test
    public void loopSupport() throws PathNotFoundException
    {
        Graph<Integer, Edge<Integer>> graph = new DefaultGraph<>(true);
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addVertex(3);
        graph.addEdge(new DefaultEdge<>(1, 2));
        graph.addEdge(new DefaultEdge<>(2, 3));
        graph.addEdge(new DefaultEdge<>(3, 1));
        Assert.assertEquals(Arrays.asList(2, 3, 1), graph.getPath(1, 1));
    }

    @Test
    public void loopSupportToSelf() throws PathNotFoundException
    {
        Graph<Object, Edge<Object>> graph = new DefaultGraph<>(true);
        Object self = new Object();
        graph.addVertex(self);
        graph.addEdge(new DefaultEdge<>(self, self));
        Assert.assertEquals(Collections.singletonList(self), graph.getPath(self, self));
    }

    @Test
    public void loopPrevention() throws PathNotFoundException
    {
        Graph<Integer, Edge<Integer>> graph = new DefaultGraph<>(true);
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addVertex(3);
        graph.addEdge(new DefaultEdge<>(1, 1));
        graph.addEdge(new DefaultEdge<>(1, 2));
        graph.addEdge(new DefaultEdge<>(2, 3));
        Assert.assertEquals(Arrays.asList(2, 3), graph.getPath(1, 3));
    }

    @Test(expectedExceptions = PathNotFoundException.class, expectedExceptionsMessageRegExp = "Path between 2 and 6 not found")
    public void getPathDirectedGraphError() throws PathNotFoundException
    {
        Graph<Integer, Edge<Integer>> graph = new DefaultGraph<>(true);
        graph.addVertex(4);
        graph.addVertex(8);
        graph.addVertex(6);
        graph.addVertex(2);
        graph.addVertex(1);
        graph.addVertex(5);
        graph.addEdge(new DefaultEdge<>(4, 6));
        graph.addEdge(new DefaultEdge<>(8, 4));
        graph.addEdge(new DefaultEdge<>(6, 1));
        graph.addEdge(new DefaultEdge<>(8, 2));
        graph.addEdge(new DefaultEdge<>(1, 2));
        graph.addEdge(new DefaultEdge<>(5, 1));
        graph.addEdge(new DefaultEdge<>(2, 5));
        graph.getPath(2, 6);
    }

    @Test(expectedExceptions = PathNotFoundException.class, expectedExceptionsMessageRegExp = "Path between 1 and 7 not found")
    public void getPathUndirectedGraphError() throws PathNotFoundException
    {
        Graph<Integer, Edge<Integer>> graph = new DefaultGraph<>();
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addVertex(3);
        graph.addVertex(4);
        graph.addVertex(5);
        graph.addVertex(6);
        graph.addVertex(7);
        graph.addEdge(new DefaultEdge<>(1, 2));
        graph.addEdge(new DefaultEdge<>(1, 3));
        graph.addEdge(new DefaultEdge<>(3, 4));
        graph.addEdge(new DefaultEdge<>(4, 1));
        graph.addEdge(new DefaultEdge<>(4, 5));
        graph.addEdge(new DefaultEdge<>(2, 6));
        graph.addEdge(new DefaultEdge<>(6, 5));
        // graph.addEdge(new DefaultEdge<>(6, 7));
        graph.getPath(1, 7);
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "Vertex 1 already exists in graph")
    public void addAlreadyExistsVertex()
    {
        Graph<Integer, Edge<Integer>> graph = new DefaultGraph<>();
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addVertex(1);
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "Edge between 2 and 1 already exists in graph. Is graph directed: false")
    public void addAlreadyExistsEdge()
    {
        Graph<Integer, Edge<Integer>> graph = new DefaultGraph<>();
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addEdge(new DefaultEdge<>(1, 2));
        graph.addEdge(new DefaultEdge<>(2, 1));
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "Source 3 does not exist in graph")
    public void sourceDoesNotExists() throws PathNotFoundException
    {
        Graph<Integer, Edge<Integer>> graph = new DefaultGraph<>(true);
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addEdge(new DefaultEdge<>(1, 2));
        graph.getPath(3, 1);
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "Destination 3 does not exist in graph")
    public void destinationDoesNotExists() throws PathNotFoundException
    {
        Graph<Integer, Edge<Integer>> graph = new DefaultGraph<>(true);
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addEdge(new DefaultEdge<>(1, 2));
        graph.getPath(1, 3);
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "Vertex 3 does not exist in graph")
    public void addEdgeWithIllegalSource()
    {
        Graph<Integer, Edge<Integer>> graph = new DefaultGraph<>(true);
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addEdge(new DefaultEdge<>(3, 1));
    }

    @Test(expectedExceptions = IllegalArgumentException.class, expectedExceptionsMessageRegExp = "Vertex 3 does not exist in graph")
    public void addEdgeWithIllegalDestination()
    {
        Graph<Integer, Edge<Integer>> graph = new DefaultGraph<>(true);
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addEdge(new DefaultEdge<>(1, 3));
    }
}